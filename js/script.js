(function () {
    let words = [
        ' your parent',
        ' your child',
        ' your partner',
        ' your friend',
        ' yourself'
    ]

    let i = 0

    setInterval(function () {
        let changingWordElement = document.getElementById('changingWord')

        fadeOut(changingWordElement, function () {
            changingWordElement.innerHTML = words[i = (i + 1) % words.length]
            fadeIn(changingWordElement)
        })
    }, 5000)

    function fadeOut(element, callback) {
        let opacity = 1
        let fadeOutInterval = setInterval(function () {
            if (opacity > 0) {
                opacity -= 0.1
                element.style.opacity = opacity
            } else {
                clearInterval(fadeOutInterval)
                callback()
            }
        }, 100)
    }

    function fadeIn(element) {
        let opacity = 0
        let fadeInInterval = setInterval(function () {
            if (opacity < 1) {
                opacity += 0.1
                element.style.opacity = opacity
            } else {
                clearInterval(fadeInInterval)
            }
        }, 100)
    }
})()